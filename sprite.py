import os
import sys

import pygame


class Sprite(pygame.sprite.Sprite):
  """ Базовый класс спрайта """

  def __init__(self, *group):
    super().__init__(*group)

  @staticmethod
  def load_image(filename):
    if not os.path.isfile(filename):
      print(f"Файл с изображением '{filename}' не найден")
      sys.exit()
    return pygame.image.load(filename)
