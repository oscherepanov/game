import pygame

from sprite import Sprite


class Score(Sprite):


  MAX_VALUE = 1000

  def __init__(self, *group):
    super().__init__(group)
    self.value = 0
    self.font = pygame.font.Font('font/pockc.ttf', 80)
    self.render()

  def inc(self):
    self.value = (self.value + 1) % self.MAX_VALUE
    self.render()

  def render(self):
    text = self.font.render(str(self.value).rjust(3, '0'), False, pygame.color.Color('black'))
    self.image = pygame.Surface((110, 80),  pygame.SRCALPHA)
    W = text.get_width()
    H = text.get_height()
    self.image.blit(text, [110 / 2 - W / 2, 110 / 2 - H / 2])
    self.rect = self.image.get_rect()
    self.rect.x = 1000
    self.rect.y = 10

  def clear(self):
    self.value = 0
    self.render()

  def get_value(self):
    return self.value
