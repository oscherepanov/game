import pygame

from sprite import Sprite


class Chicken(Sprite):
  """ Класс цыпленка, который выбегает при разбитии яйца """

  first_image = Sprite.load_image('images/chick1.png')
  second_image = Sprite.load_image('images/chick2.png')

  def __init__(self, direction, *group):
    super().__init__(*group)
    self.direction = direction
    if direction == 'left':
      self.image = self.first_image
      self.rect = self.image.get_rect()
      self.rect.x = 210
    else:
      self.image = pygame.transform.flip(self.first_image, True, False)
      self.rect = self.image.get_rect()
      self.rect.x = 850
    self.rect.y = 480

    self.count = 0
    self.crash_sound = pygame.mixer.Sound('wavs/crush.wav')

  def update(self):
    if self.count == 1:
      x, y = self.rect.x, self.rect.y
      self.crash_sound.play()
      if self.direction == 'left':
        self.image = self.second_image
        self.rect = self.image.get_rect()
        self.rect.x = x - 75
      else:
        self.image = pygame.transform.flip(self.second_image, True, False)
        self.rect = self.image.get_rect()
        self.rect.x = x + 200
      self.rect.y = y
    elif 1 < self.count < 4:
      if self.direction == 'left':
        self.rect.x -= 75
      else:
        self.rect.x += 75
    elif self.count:
      self.kill()

    self.count += 1
