import random

import pygame

from board import Board
from chicken import Chicken
from egg import Egg
from life import Life
from score import Score
from volf import Volf


class Game:
  """ Главный класс игры """

  def __init__(self):
    pygame.init()
    self.catch_sound = pygame.mixer.Sound('wavs/catch.wav')

    size = 1240, 600
    self.screen = pygame.display.set_mode(size)
    self.font = pygame.font.Font('font/pockc.ttf', 80)

    self.all_sprites = pygame.sprite.Group()
    self.eggs_group = pygame.sprite.Group()
    self.checken_group = pygame.sprite.Group()

    self.board = Board(self.all_sprites)
    self.volf = Volf(self.all_sprites)

    self.score = Score(self.all_sprites)
    self.life = Life(self.all_sprites)

    self.level = 1
    self.egg_generate_rate = 15
    self.counter_egg_generator = 1

    self.normal_state = True
    self.start_flag = False

  def game_over(self):
    self.score.clear()
    self.life.clear()
    self.level = 1
    self.counter_egg_generator = 1
    for i in self.eggs_group.sprites():
      i.kill()

    self.start_flag = False

  def change_level(self):
    score = self.score.get_value()
    if score == 10:
      self.level += 1
    elif score == 50:
      self.level += 1

  def draw_text(self, text='Click the button Space'):
    text = self.font.render(text, False, pygame.color.Color('black'))
    self.screen.blit(text, (300, 0))

  def get_probabilty_by_level(self):
    if self.level == 1:
      return 0.2
    if self.level == 2:
      return 0.4
    return 0.6

  def generate_egg(self):
    rnd_value = random.random()
    if rnd_value < self.get_probabilty_by_level():
      position = random.choice(['left-up', 'left-down', 'right-up', 'right-down'])
      Egg(position, self.all_sprites, self.eggs_group)

  def check_eggs(self):
    for egg in self.eggs_group.sprites():
      if egg.is_last_state():
        if egg.get_position() == self.volf.get_position():
          self.catch_sound.play()
          self.score.inc()
          self.change_level()
        else:
          self.normal_state = False
          Chicken('left' if 'left' in egg.get_position() else 'right', self.checken_group)
          self.life.inc()
        egg.kill()

  def run(self):
    normal_fps = fps = 30
    clock = pygame.time.Clock()
    running = True
    while running:
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          running = False
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_q:
            self.volf.move_to_left_up()
          elif event.key == pygame.K_a:
            self.volf.move_to_left_down()
          elif event.key == pygame.K_p:
            self.volf.move_to_right_up()
          elif event.key == pygame.K_l:
            self.volf.move_to_right_down()
          elif event.key == pygame.K_SPACE:
            self.start_flag = True
      pygame.display.flip()
      self.all_sprites.draw(self.screen)

      if self.normal_state and self.start_flag:
        self.check_eggs()

        if not self.counter_egg_generator % self.egg_generate_rate:
          self.generate_egg()
          self.counter_egg_generator = 1

        self.all_sprites.update()
        self.counter_egg_generator += 1
      elif self.start_flag:
        if not self.checken_group.sprites():
          self.normal_state = True
          fps = normal_fps
          if self.life.is_off():
            self.game_over()
        else:
          fps = 1
          self.checken_group.draw(self.screen)
          self.checken_group.update()
      else:
        self.draw_text()
      clock.tick(fps)
    pygame.quit()
