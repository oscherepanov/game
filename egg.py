import pygame

from sprite import Sprite


class Egg(Sprite):
  """ Класс яйца """

  filename = 'images/egg.png'
  delta_x = 53
  a = 0.55

  rate = 20

  def __init__(self, position, *group):
    super().__init__(*group)
    self.sound = pygame.mixer.Sound('wavs/egg.wav')
    self.position = position

    self.image = self.load_image(self.filename)
    if 'right' in position:
      self.image = pygame.transform.flip(self.image, True, False)
    self.rect = self.image.get_rect()
    self.start_position()

    self.count = 1
    self.tick_counter = 1

  def start_position(self):
    if self.position == 'left-up':
      self.rect.x = 80
      self.rect.y = 100
    elif self.position == 'left-down':
      self.rect.x = 80
      self.rect.y = 270
    elif self.position == 'right-up':
      self.rect.x = 1120
      self.rect.y = 100
    else:
      self.rect.x = 1120
      self.rect.y = 270
    self.sound.play()

  def is_last_state(self):
    return self.count == 4

  def get_position(self):
    return self.position

  def update(self):
    if not self.tick_counter % self.rate:
      self.image = pygame.transform.rotate(self.image, 90)
      self.tick_counter = 1
      if self.count < 4:
        self.sound.play()
        if 'right' in self.position:
          self.rect.x -= self.delta_x
          if self.position == 'right-up':
            self.rect.y = int(-0.55 * self.rect.x + 716)
          else:
            self.rect.y = int(-0.55 * self.rect.x + 886)

        else:
          self.rect.x += self.delta_x
          if self.position == 'left-up':
            self.rect.y = int(0.55 * self.rect.x + 53)
          else:
            self.rect.y = int(0.55 * self.rect.x + 226)
      self.count += 1
    self.tick_counter += 1
