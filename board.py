from sprite import Sprite


class Board(Sprite):
  """ Класс экрана """
  filename = 'images/board.png'

  def __init__(self, *group):
    super().__init__(*group)
    self.image = super().load_image(self.filename)
    self.rect = self.image.get_rect()

