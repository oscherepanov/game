import pygame

from sprite import Sprite


class Volf(Sprite):
  """ Класс волка """

  volf_image_right_down = 'images/volf_down.png'
  volf_image_right_up = 'images/volf_up.png'

  y_coordinates = 160

  def __init__(self, *group):
    super().__init__(*group)
    self.image_right_down = self.load_image(self.volf_image_right_down)
    self.image_right_up = self.load_image(self.volf_image_right_up)
    self.image_left_down = pygame.transform.flip(self.image_right_down, True, False)
    self.image_left_up = pygame.transform.flip(self.image_right_up, True, False)

    self.move_to_right_up()

  def move_to_left_up(self):
    self.image = self.image_left_up
    self.rect = self.image.get_rect()
    self.rect.x = 230
    self.rect.y = self.y_coordinates
    self.position = 'left-up'

  def move_to_left_down(self):
    self.image = self.image_left_down
    self.rect = self.image.get_rect()
    self.rect.x = 230
    self.rect.y = self.y_coordinates
    self.position = 'left-down'

  def move_to_right_up(self):
    self.image = self.image_right_up
    self.rect = self.image.get_rect()
    self.rect.x = 675
    self.rect.y = self.y_coordinates
    self.position = 'right-up'

  def move_to_right_down(self):
    self.image = self.image_right_down
    self.rect = self.image.get_rect()
    self.rect.x = 675
    self.rect.y = self.y_coordinates
    self.position = 'right-down'

  def get_position(self):
    return self.position
