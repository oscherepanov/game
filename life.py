import pygame

from sprite import Sprite


class Life(Sprite):
  """ Класс отображения разбитых яиц """


  source_image = Sprite.load_image('images/live.png')

  def __init__(self, *group):
    super().__init__(*group)
    self.count = 0
    self.create_image()
    self.render()

  def create_image(self):
    self.image = pygame.Surface((300, 100),  pygame.SRCALPHA)
    self.rect = self.image.get_rect()
    self.rect.x = 700
    self.rect.y = 100

  def render(self):
    if self.count >= 1:
      self.image.blit(self.source_image, (0, 0))
    if self.count >= 2:
      self.image.blit(self.source_image, (100, 0))
    if self.count >= 3:
      self.image.blit(self.source_image, (200, 0))

  def inc(self):
    self.count += 1
    self.render()

  def clear(self):
    self.count = 0
    self.create_image()
    self.render()

  def is_off(self):
    return self.count == 3
